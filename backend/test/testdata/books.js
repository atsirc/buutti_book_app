const books = [
  {
    'name': 'Unlocking Android',
    'id': '1933988673',
    'author': 'W. Frank Ableson',
    'read': true
  },
  {
    'name': 'Android in Action, Second Edition',
    'id': '1935182722',
    'author': 'W. Frank Ableson',
    'read': false
  },
  {
    'name': 'Specification by Example',
    'id': '1617290084',
    'author': 'Gojko Adzic',
    'read': true
  },
  {
    'name': 'Flex 3 in Action',
    'id': '1933988746',
    'author': 'Tariq Ahmed with Jon Hirschi',
    'read': false
  },
  {
    'name': 'Flex 4 in Action',
    'id': '1935182420',
    'author': 'Tariq Ahmed',
    'read': false
  },
  {
    'name': 'Zend Framework in Action',
    'id': '1933988320',
    'author': 'Rob Allen',
    'read': true
  }
];

export {books as default};
