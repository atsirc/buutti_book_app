import {jest} from '@jest/globals';
import request from 'supertest';
import app from '../src/index.js';
import {pool} from '../src/db/db.js';
import books from './testdata/books.js';
import bcrypt from 'bcrypt';

const initMock = (mockFunc) => {
  pool.connect = jest.fn(()=>{
    return {
      query: mockFunc,
      release: () => null
    };
  });
}

const userLogin = {
  'username': 'TestUser',
  'password': 'test'
};

const mockUser = {
    'id': '78shkjnsd98kh930980-aslkj',
    'username': userLogin.username
};

const headers = {};

beforeAll(async() => {
  const passhash = await bcrypt.hash(userLogin.password, 10);
  const mockresponse = { rows: [{...mockUser, passhash}] };
  initMock(() => mockresponse);
  const response = await request(app)
     .post('/user/login')
     .send(userLogin);
  headers['authorization'] = 'bearer ' + response.body.token;
});

describe('GET books', () => {

  it('get all books returns 200 and response matches test response', async () => {
    const mockResponse = {
      rows: [...books]
    };

    initMock(() => mockResponse);

    const response = await request(app).get('/books/').set(headers);

    expect(response.status).toBe(200);
    expect(JSON.parse(response.text)).toStrictEqual(mockResponse.rows);
  });

  it('get specific book with id returns 200 and an object that matches test-object', async () => {
    const mockResponse = {
      rows: [{...books[0]}]
    };

    initMock(() => mockResponse);

    const response = await request(app)
      .get('/books/' + books[0]['id']);

    expect(response.status).toBe(200);
    expect(response.body).toMatchObject(books[0]);
  });

  it('get specific book with id 193398673 that doesn\'t exist returns 400', async () => {
    const mockResponse = {
      rows: []
    };

    initMock(() => mockResponse);

    await request(app)
      .get('/books/193398673')
      .expect(400);
  });
});

describe('POST books', () => {
  it('creating a new book returns 201', async () => {
    const newBook = {
      name: 'this name',
      author: 'some author',
      read: true 
    };

    const mockResponse = {
      rows: [{id: '2390flks92', ...newBook}]
    };

    initMock(() => mockResponse);

    await request(app)
      .post('/books/')
      .set(headers)
      .send(newBook)
      .expect(201);
  });

  it('creating a new book without all keys returns 400', async () => {
    const newBook = {
      name: 'this name',
      author: 'some author'
    };

    /* db is never called in this test */

    await request(app)
      .post('/books/')
      .set(headers)
      .send(newBook)
      .expect(400);
  });

  it('creating a book with string instead of boolean as value for key `read` returns 400', async () => {
    const newBook = {
      name: 'this name',
      author: 'some author',
      read: 'true'
    };

    /* database is never called in this test */

    await request(app)
      .post('/books/')
      .set(headers)
      .send(newBook)
      .expect(400);
  });
});

describe('PUT books', () => {
  it('updating a book with a non-existing id 193518222, return 400', async () => {
    const newBook = {
      name: 'this name',
      author: 'some author',
    };

    // bookService does call db twice for updateBook-function but,
    // in this case, where the id doesn't exist there will only be one call to db before error is thrown
    const mockresponse = {
      rows: []
    };

    initMock(() => mockresponse);

    await request(app)
      .put('/books/193518222') 
      .set(headers)
      .send(newBook)
      .expect(400);
  });

  it('updating a book with id 1617290084 only the name should be changed', async () => {
    const updatedInfo = {
      author: 'some author',
    };

    // bookService updateBook-function calls db twice so 2 results are needed
    const mockresponse = [
      { rows: [{...books[2]}]},
      { rows: [{...books[2], author: updatedInfo.author}]}
    ];

    initMock(() => mockresponse.shift());

    const expectedBook = {...books[2], 'author': 'some author'};
    const response = await request(app)
      .put('/books/1617290084')
      .set(headers)
      .send(updatedInfo)
     
    expect(response.status).toBe(200)
    expect(response.body).toMatchObject(expectedBook)
  });

  it('updating a book with id 1617290084 with string as type for key ´read´ returns 400', async () => {

    const updatedInfo = {
      read: 'true',
    };

    /* database is not called in this test */

    const response = await request(app)
      .put('/books/1617290084')
      .set(headers)
      .send(updatedInfo);

    expect(response.status).toBe(400);
  });
});

describe('DELETE books', () => {

  it('deleting a book not in database returns 400', async () =>  {
    const mockresponse = {
      rows: []
    };

    initMock(() => mockresponse);
    await request(app)
      .delete('/books/2093')
      .set(headers)
      .expect(400);
  });

  it('deleting book in db returns 200', async () =>  {
    const mockresponse = {
      rows: [{...books[0]}]
    };

    initMock(() => mockresponse);

    const response = await request(app)
      .delete('/books/'+books[0]['id']).set(headers);

    expect(response.status).toBe(200);
    expect(response.body).toMatchObject(mockresponse.rows[0]);
  });
});
