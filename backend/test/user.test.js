import {jest} from '@jest/globals';
import request from 'supertest';
import app from '../src/index.js';
import {pool} from '../src/db/db.js'
import bcrypt from 'bcrypt';

const userLogin = {
  'username': 'TestUser',
  'password': 'test'
};

const mockUser = {
    'id': '78shkjnsd98kh930980-aslkj',
    'username': userLogin.username
};

let passhash = '';
beforeAll(async() => {
  passhash = await bcrypt.hash(userLogin.password, 10);
});

const initMock = (mockFunc) => {
  pool.connect = jest.fn(()=> {
    return {
      query: mockFunc,
      release: () => null
    };
  });
}

describe('POST user tests', () => {
  it('creating a new user returns object that matches test-case', async () => {

    const mockresponse = [
      { rows: [] },
      { rows: [{...mockUser, passhash}] }
    ];

    initMock(() => mockresponse.shift());

    const response = await request(app)
      .post('/user/create')
      .send(userLogin);
    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('username', userLogin.username);
    expect(response.body).toHaveProperty('id');
  });

  it('trying to create another user with same username return 400 and the message `is already taken`', async () => {

    const mockresponse = { rows: [{...mockUser, passhash}] };

    initMock(() => mockresponse);
    const response = await request(app)
      .post('/user/create')
      .send(userLogin);

    expect(response.statusCode).toBe(400);
    expect(response.text).toMatch('is already taken');
  });

  it('creating a new user without password fails with 400', async () => {
    /* doesnt need mock db because fails before call to db */
    const response = await request(app)
      .post('/user/create')
      .send({
        'username': userLogin.username
      });

    expect(response.statusCode).toBe(400);
  });

  it('test login in with recently created user returns 200 and a token', async () => {

    const mockresponse = { rows: [{...mockUser, passhash}] };

    initMock(() => mockresponse);

    const response = await request(app)
      .post('/user/login')
      .send(userLogin);

    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('token');
  });
  
  it('test logging in with wrong password returns message that contains word `incorrect`', async () => {

    const mockresponse = { rows: [{...mockUser, passhash}] };

    initMock(() => mockresponse);

    const response = await request(app)
      .post('/user/login')
      .send({...userLogin, 'password': 'wrong'});

    expect(response.statusCode).toBe(400);
    expect(response.text).toMatch('incorrect');
  });

  it('test logging in with non-existant user returns 400', async () => {
    const mockresponse = { rows: [] };

    initMock(() => mockresponse);
    const response = await request(app)
      .post('/user/login')
      .send({
        'username': 'christa',
        'password': 'test'
      });

    expect(response.statusCode).toBe(400);
  });
});
