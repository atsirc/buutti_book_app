import { Router } from 'express';
import service from '../services/userService.js';

const router = Router();

router.post('/create', async(req, res, next) => {
  try {
    const result = await service.createUser(req.body);
    res.status(201).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/login', async(req, res, next) => {
  try {
    const token = await service.login(req.body);
    res.status(200).json({ token });
  } catch (error) {
    next(error);
  }
});

export default router;
