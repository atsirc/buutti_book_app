import express from 'express';
import service from '../../services/bookService.js';
import authorize from '../../middleware/authorize.js';
const router = express.Router();

const validateBook = (req, _res, next) => {
  const body = req.body;
  if (['name', 'author', 'read'].every(key => body.hasOwnProperty(key))) {
    if (typeof body.read === 'boolean') {
      return next();
    }
  }
  // I think this works without next, because this is not an error within async function
  throw new Error('required keys are `name` [string], `author` [string] and `read` [boolean]');
};

router.get('/', authorize, async (req, res, next) => {
  try {
    const user = req.authUser;
    const books = await service.findAllUsersBooks(user.id);
    res.status(200).json(books);
  } catch (error) {
    next(error);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const book = await service.findOne(req.params.id);
    res.status(200).json(book);
  } catch (error) {
    next(error);
  }
});

router.post('/', validateBook, authorize, async (req, res, next) => {
  const book = req.body;
  book.user_id = req.authUser.id;
  try {
    const result = await service.insertBook(book);
    res.status(201).json(result);
  } catch (error) {
    next(error);
  }
});

router.put('/:id', authorize, async (req, res, next) => {
  try {
    const id = req.params.id;
    const modifiedBook = req.body;
    const result = await service.updateBook(id, modifiedBook);
    res.status(200).json(result);
  } catch(error) {
    next(error);
  }
});

router.delete('/:id', authorize, async (req, res, next) => {
  try {
    const id = req.params.id;
    const result = await service.deleteBook(id);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

export default router;
