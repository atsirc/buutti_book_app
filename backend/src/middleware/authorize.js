import 'dotenv/config';
import jwt from 'jsonwebtoken';

const authorize = (req, _res, next) => {
  try {
    const auth = req.headers['authorization'];
    const token = auth.replace('bearer ', '');
    const authUser = jwt.verify(token, process.env.APP_SECRET);
    req.authUser = authUser;
    next();
  } catch (error) {
    throw error;
  }
};

export default authorize;
