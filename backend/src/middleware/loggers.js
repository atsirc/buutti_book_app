export const requestLogger = (req, res, next) => {
  const method = req.method;
  const url = req.url;
  const status = res.statusCode;
  console.log('------------------------------------------------------------');
  console.log(`${new Date().toLocaleString()} ${method}:${url} ~ ${status}`);
  next();
};

export const errorLogger = (err, _req, _res, next) => {
  console.log('------------------------------------------------------------');
  console.log('Error: ', err.message);
  next(err);
};
