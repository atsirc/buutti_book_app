const addUser = 'INSERT INTO users ("id", "username", "passhash") VALUES ($1,$2,$3) RETURNING *;';

const getUser = 'SELECT * FROM users WHERE username=$1;';

const createBookTable = `
  CREATE TABLE IF NOT EXISTS books (
    "id" VARCHAR(36) NOT NULL UNIQUE, 
    "name" VARCHAR(255) NOT NULL,
    "author" VARCHAR(255) NOT NULL,
    "read" BOOLEAN NOT NULL,
    "user_id" VARCHAR(255), 
    PRIMARY KEY ("id"));`;

const createUserTable = `
  CREATE TABLE IF NOT EXISTS users (
    "id" VARCHAR(36) NOT NULL UNIQUE,
    "username" VARCHAR(255) NOT NULL UNIQUE,
    "passhash" VARCHAR(255) NOT NULL,
    PRIMARY KEY ("id"));`;

const deleteBook = 'DELETE FROM books WHERE id=$1 RETURNING *;';

const getBook = 'SELECT * FROM books WHERE id=$1;';

const getAllBooks = 'SELECT * FROM books;';

const getUsersBooks = 'SELECT * FROM books WHERE user_id=$1;';

const insertBook = `
  INSERT INTO books 
    ("id", "name", "author", "read", "user_id")
    VALUES ($1,$2,$3,$4,$5) 
  RETURNING *;`;

const updateBook = `
  UPDATE books SET 
      name=$2, author=$3, read=$4, user_id=$5 
  WHERE id=$1 RETURNING *;`;

export default {
  addUser,
  getUser,
  createBookTable,
  createUserTable,
  deleteBook,
  getBook,
  getUsersBooks,
  getAllBooks,
  insertBook,
  updateBook
};
