import dotenv from 'dotenv';
dotenv.config();
import requests from '../dao/userDao.js';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

const createUser = async user => {
  if (!user.password || !user.username) {
    throw new Error('Required keys are usesername and password');
  }
  const alreadyInDb = await requests.getUserByUsername(user.username);
  if (typeof alreadyInDb !== 'undefined') {
    throw new Error('Username is already taken.');
  }
  const saltRounds = 10;
  const passhash = await bcrypt.hash(user.password, saltRounds);
  const storableUser = {
    username: user.username,
    passhash
  };
  const newUser = await requests.insertUser(storableUser);
  if (typeof newUser !== 'undefined') {
    return {id: newUser.id, username: newUser.username};
  } else {
    throw new Error('Something went wrong!');
  }
};

const login = async user => {
  const userFromDb = await requests.getUserByUsername(user.username);
  if (typeof userFromDb !== 'undefined' && user) {

    const passCorrect = await bcrypt.compare(user.password, userFromDb.passhash);
    if (!passCorrect) {
      throw new Error('username or password or both are incorrect');
    }

    const token = jwt.sign({
      id: userFromDb.id,
      username: user.username
    }, process.env.APP_SECRET);
    return token;
  } else {
    throw new Error('username or password or both are incorrect');
  }
};

export default {
  createUser,
  login
};
