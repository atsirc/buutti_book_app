import {v4} from 'uuid';
import db from '../db/db.js';
import queries from '../db/queries.js';

const handleError = (response) => {
  if (typeof response === 'undefined') {
    throw new Error('Something went wrong');
  } else {
    return response;
  }
};

const findAll = async () => {
  const result = await db.execQuery( queries.getAllBooks );
  return handleError(result.rows);
};

const findAllUsersBooks = async (userId) => {
  const result = await db.execQuery( queries.getUsersBooks, [userId] );
  return handleError(result.rows);
};

const findOne = async (id) => {
  const result = await db.execQuery( queries.getBook, [id] );
  return handleError(result.rows[0]);
};

const insertBook = async (book) => {
  // this is for removing unwanted keys
  const newBook = {
    name: book.name,
    author: book.author,
    read: book.read,
    user_id: book.user_id || null
  };

  const params = [v4(), ...Object.values(newBook)];
  const result = await db.execQuery( queries.insertBook, params );
  return handleError(result.rows[0]);
};

const updateBook = async (id, modifiedBook) => {
  if (modifiedBook.read && typeof modifiedBook.read !== 'boolean') {
    throw new Error('wrong values');
  }
  const book = await findOne(id);
  book.name = modifiedBook.name || book.name;
  book.author = modifiedBook.author || book.author;
  // if you change read value from true to false, the above method of assigning values would fail.
  if (Object(modifiedBook).hasOwnProperty('read')) {
    book.read = modifiedBook.read;
  }
  const params = Object.values(book);
  const result = await db.execQuery( queries.updateBook, params );
  return handleError(result.rows[0]);
};

const deleteBook = async(id) => {
  const result = await db.execQuery( queries.deleteBook, [id] );
  return handleError(result.rows[0]);
};

export default {
  findAll,
  findAllUsersBooks,
  findOne,
  insertBook,
  updateBook,
  deleteBook
};
