import {v4} from 'uuid';
import db from '../db/db.js';
import queries from '../db/queries.js';

const insertUser = async (user) => {
  const params = [v4(), ...Object.values(user)];
  const result = await db.execQuery( queries.addUser, params );
  return result.rows[0];
};

const getUserByUsername = async (username) => {
  const result = await db.execQuery( queries.getUser, [username] );
  return result.rows[0];
};

export default {
  insertUser,
  getUserByUsername
};
