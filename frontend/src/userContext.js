import { useState, useEffect, createContext } from 'react';

export const UserContext = createContext([{}, () => {}]);

export const UserProvider = (props) => {

  const [user, setUser] = useState({});

  useEffect(() => {
    try {
      const user = localStorage.getItem('user');
      const parsed = JSON.parse(user);
      setUser(parsed);
    } catch (error) {
      console.log(error)
    }
  }, []);

  return (
    <UserContext.Provider value={[user, setUser]}>
      {props.children}
    </UserContext.Provider>
  )
}
