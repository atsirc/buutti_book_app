import './App.css';
import {useState} from 'react';
import { Alert } from '@mui/material';
import { UserProvider } from './userContext';
import Page from './Page.js';

const App = () => {
  const [message, setMessage] = useState({
    variant: '',
    message: ''
  });

  const addAlert = (variant, message) => {
    console.log(variant, message)
    setMessage(_prev => (
      {
        variant: variant,
        message: message
      }
    ));
    setTimeout(()=> {
      setMessage(_prev => ({
        variant: '',
        message: ''
      }));
    }, 5000);
  };

  return (
    <UserProvider>
      <div className="App">
        {message['variant'].length > 0 && <Alert style={{width: '100%', position: 'absolute', top: '0'}} severity={message.variant} >{message.message}</Alert>}
        <Page addAlert={addAlert}/>
      </div>
    </UserProvider>
  );
}

export default App;
