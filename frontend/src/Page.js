import {useContext} from 'react';
import {Button} from '@mui/material';
import {UserContext } from './userContext';
import BookView from './BookView.js';
import LoginForm from './LoginForm.js';

const Page = ({addAlert}) => {
  const [user, setUser] = useContext( UserContext );

  const logout = () => {
    setUser('');
    localStorage.removeItem('user');
  };

  return (
    <div style={{marginTop: '3em'}}>
      { user ? 
      <>
        <h1>{user.username}'s books<Button style={{ width: '7em', margin: '1em' }} onClick={logout}>Log out</Button></h1>
        <BookView addAlertMessage={addAlert}/> 
      </>
      : <LoginForm addAlertMessage={addAlert} setUser={setUser}/>
      }
    </div>
  );
};

export default Page;
