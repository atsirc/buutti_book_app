import {useContext, useState} from 'react';
import {Grid, Button, Paper} from '@mui/material';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { UserContext } from './userContext.js';
import axios from 'axios';

const LoginForm = ({addAlertMessage}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  // eslint-disable-next-line
  const [user, setUser] = useContext(UserContext);
  const [createAccount, setCreateAccount] = useState(false)

  const updateValue = ({target}) => {
    switch (target.id) {
      case 'username': 
       setUsername(target.value); 
        break;
      case 'password': 
        setPassword(target.value); 
        break;
      default:
        break;
    }
  };

  const toggleCreateAccount = () => {
    setCreateAccount( prev => !prev );
    setUsername('');
    setPassword('');
  }
  const addAccount = async () => {
    try {
      const result = await axios.post('/user/create', {username: username, password: password});
      const user = result.data;
      addAlertMessage('success', `${user.username} has successully been added! You can now login!`)
      setCreateAccount(false);
    } catch (error) {
      addAlertMessage('error', error.response.data);
    }
  }
  const login = async () => {
    try {
      const result = await axios.post('/user/login', {username: username, password: password});
      console.log(result);
      const userToken = result.data;
      const user = {
        ...userToken,
        username: username,
      };
      localStorage.setItem('user', JSON.stringify(user));
      addAlertMessage('success', `Welcome back!`)
      setUser(user);
    } catch (error) {
      addAlertMessage('error', error.response.data);
    }
  }

  return (
    <Paper id="loginForm" style={{width: '60vw', margin: 'auto'}}>
      <h1>{createAccount ? 'Create account' : 'Login'}</h1>
      <ValidatorForm onSubmit={createAccount ? addAccount : login} onError={error => console.log(error)}>
      <Grid>
        <Grid item>
          <TextValidator fullWidth id="username" label="Username" onChange={updateValue} name="username" value={username || ''} validators={['required',`matchRegexp:^[\\w\\W]{3,}$`]} errorMessages={['this field is required', 'At least 3 characters are required.']} />
        </Grid>
        <Grid item>
          <TextValidator type="password" fullWidth id="password" label="Password" onChange={updateValue} name="username" value={password || ''} validators={['required',`matchRegexp:^[\\w\\W]{3,}$`]} errorMessages={['this field is required', 'At least 3 characters are required.']} />
        </Grid>
        <Grid item>
          <Button variant="contained" type="submit">{createAccount ? 'Create account' : 'Login'}</Button>
        </Grid>
      </Grid>
      </ValidatorForm>
    {
      createAccount ? 
        <Button id="extra-button" onClick={toggleCreateAccount}>or login to your account!</Button> :
        <Button id="extra-button" onClick={toggleCreateAccount}>or create an account!</Button>
      }
    </Paper>
  )
}

export default LoginForm;
