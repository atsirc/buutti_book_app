import {useContext, useEffect, useState} from 'react';
import {Button, TableContainer, Table, TableHead, TableRow, TableBody, TableCell, Paper, Checkbox} from '@mui/material';
import axios from 'axios';
import {UserContext} from './userContext.js';
import {BookForm} from './BookForm.js';

const BookView = ({addAlertMessage}) => {
  const [books, setBooks] = useState([]);
  // eslint-disable-next-line
  const [user, setUser] = useContext(UserContext);

  useEffect(() => {
    const getBooks = async() => {
      if (user.hasOwnProperty('token')) {
        const result= await axios.get('/books', { headers: { authorization: 'bearer ' + user.token }});
        console.log(result.data)
        setBooks(result.data)
      }
    }
    getBooks();
  }, [user]);

  const checkRead = async (book) => {
    try {
      book.read = !book.read;
      const result = await axios.put('/books/' + book.id, book, {headers: {authorization: 'bearer ' + user.token}});
      const updatedBook = result.data;
      setBooks(prev => 
        prev.map(book => book.id === updatedBook.id ? updatedBook : book)
      );
      addAlertMessage('success', `${updatedBook.author}'s ${updatedBook.name} was updated successfully`);
    } catch (error) {
      addAlertMessage('error', error.response.data)
    }
  };

  const remove = async (book) => {
    try {
      const result = await axios.delete('/books/' + book.id, {headers: {authorization: 'bearer ' + user.token}});
      const removedBook = result.data;
      setBooks(prev => prev.filter(book => book.id !== removedBook.id));
      addAlertMessage('success', `${removedBook.author}'s ${removedBook.name} was removed successfully`);
    } catch (error) {
      addAlertMessage('error', error.response.data);
    }
  }

  return (
    <>
      <TableContainer className="table" component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                Author
              </TableCell>
              <TableCell>
                Title
              </TableCell>
              <TableCell>
                Read
              </TableCell>
              <TableCell>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
        {
          books.length > 0 && books.map(book => {
            return (
            <TableRow key={book.id}>
              <TableCell>{book.author}</TableCell>
              <TableCell>{book.name}</TableCell>
              <TableCell><Checkbox checked={book.read} defaultValue={false} onChange={() => checkRead(book)}/></TableCell>
              <TableCell><Button onClick={() => remove(book)}>Remove</Button></TableCell>
            </TableRow>);
          })
        }
          </TableBody>
        </Table>
      </TableContainer>
      <BookForm setMessage={addAlertMessage} setBooks={setBooks}/>
    </>
  );
}

export default BookView;
