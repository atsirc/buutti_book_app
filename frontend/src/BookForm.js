import axios from 'axios';
import { useState, useContext } from 'react';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { Button, Paper, Grid } from '@mui/material';
import { UserContext } from './userContext.js';

export const BookForm = ({setMessage, setBooks}) => {
  // eslint-disable-next-line  
  const [user, setUser] = useContext(UserContext);

  const [title, setTitle] = useState('');
  const [author, setAuthor] = useState('');

  const updateValue = ({target}) => {
    switch (target.id) {
      case 'name': 
       setTitle(target.value); 
        break;
      case 'author': 
        setAuthor(target.value); 
        break;
      default:
        break;
    }
  };

  const addBook = async () => {
    try {
      const result = await axios.post('/books', {name: title, author: author, read: false}, {headers: { authorization: 'bearer ' + user.token }} );
      setTitle(_prev => '');
      setAuthor(_prev => '');
      const newBook = result.data;
      setBooks(prev => ([...prev, newBook]));
      setMessage('success', `${newBook.author}'s ${newBook.name} was added successfully`);
    } catch (error){
      setMessage('error', error.response.data);
    }
  }

  return (
    <Paper elevation={0} style={{width: '80vw', margin: 'auto', marginTop: '3em'}} >
      <ValidatorForm onSubmit={addBook} onError={error => console.log(error)}>
        <Grid container spacing={4}>
        <Grid item xs={4}>
          <TextValidator fullWidth variant="standard" id="author" label="Author" onChange={updateValue} name="author" value={author || ''} validators={['required',`matchRegexp:^[\\w\\W]{3,}$`]} errorMessages={['this field is required', 'At least 3 characters are required.']} />
        </Grid>
        <Grid item xs={6}>
          <TextValidator fullWidth variant="standard" id="name" label="Title" onChange={updateValue} name="name" value={title || ''} validators={['required',`matchRegexp:^[\\w\\W]{3,}$`]} errorMessages={['this field is required', 'at least 3 charachters are required']} />
        </Grid>
        <Grid item xs={2}>
          <Button variant="contained" type="submit">Submit</Button>
        </Grid>
    </Grid>
      </ValidatorForm>
    </Paper>
  )
};
